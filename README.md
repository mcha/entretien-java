### Description

## Exercices d'entretien

Ce module regroupe un ensemble d'exercices rapides pouvant être proposées aux candidats.

L'objectif est de juger le candidat sur la qualité du raisonnement, du code et des pratiques mises en oeuvre.

Note : prévoir d'ouvrir ce module sous IntelliJ et Eclipse en fonction des préférences du candidat.

### Description

* Niveau débutant : PersonTest (exercice simple)
* Niveau intermédiaire : TradingTest (plusieurs questions)
* Niveau avancé : PythagoreTripleTest (nécessite un peu plus de réflexion)

### Dépendances

Ce module met à disposition :

* JAVA 8
* JUnit 4 + AssertJ 3.x
* SLF4J 1.x + Logback 1.x

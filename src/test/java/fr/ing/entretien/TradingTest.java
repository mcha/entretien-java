package fr.ing.entretien;

import java.util.Arrays;
import java.util.List;

public class TradingTest {

    private static class Trader{

        private String name;
        private String city;

        public Trader(String n, String c){
            this.name = n;
            this.city = c;
        }

        public String getName(){
            return this.name;
        }

        public String getCity(){
            return this.city;
        }

        public void setCity(String newCity){
            this.city = newCity;
        }

        public String toString(){
            return "Trader:"+this.name + " in " + this.city;
        }
    }

    private static class Transaction{

        private Trader trader;
        private int year;
        private int value;

        public Transaction(Trader trader, int year, int value)
        {
            this.trader = trader;
            this.year = year;
            this.value = value;
        }

        public Trader getTrader(){
            return this.trader;
        }

        public int getYear(){
            return this.year;
        }

        public int getValue(){
            return this.value;
        }

        public String toString(){
            return "{" + this.trader + ", " +
                    "year: "+this.year+", " +
                    "value:" + this.value +"}";
        }
    }

    public List<Transaction> givenTransactions() {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");
        Trader john = new Trader("John", "Hong-Kong");

        return Arrays.asList(
                	new Transaction(raoul, 2011, 400),
                new Transaction(raoul, 2012, 1000),
                	new Transaction(mario, 2011, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950),
                	new Transaction(brian, 2011, 300),
                	new Transaction(john, 2011, 400)
        );
    }

    ////////////////////////// TESTS
    
    
    // Etant donnée le modèle ci-dessus et la liste de transactions fournie

    // 1 : Coder une méthode qui retourne toutes les transactions de 2011
    // -> la liste doit être trier par valeur decroissante
    
    // 2 : Refactorer la réponse à la question 1, si deux transactions ont la même valeur
    // alors le tri doit se faire sur le nom du trader

    // 3 : Coder une méthode qui retourne les villes (sans doublon) où travaillent les traders


}

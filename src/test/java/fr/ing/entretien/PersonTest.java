package fr.ing.entretien;

import java.util.Arrays;
import java.util.List;

public class PersonTest {

    private static class Person {
        String firstName;
        Integer age;

        Person(String firstName, Integer age) {
            this.firstName = firstName;
            this.age = age;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

    }

    private List<Person> given_persons() {
        return Arrays.asList(
                new Person("Jean",24), new Person("Marc",18),
                new Person("Helene",10), new Person("John",5),
                new Person("Yann",32), new Person("Nicole",11));
    }

    //Ecrire une methode qui retourne les prénoms en majuscule des personnes majeurs

}

package fr.ing.entretien;

public class PythagoreTripleTest {

    // Rappelez vous le théorème de pythagore :
    // Dans un triangle rectangle, le carré de l'hypothynuse est égale à la somme des carrés des côtés opposés

    // Un "triplet de Pythagore" est un triplet de 3 entiers (a,b,c) qui vérifie cette relation
    // a*a + b*b = c*c
    // Ex: (3,4,5) est un triplet de Pythagore car 9 + 16 = 25

    // Pour "a" commençant à 1, retrouver les 6 premiers triplet (a,b,c) de Pythagore
    // Tips : un nombre n est entier si n % 1 == 0

}

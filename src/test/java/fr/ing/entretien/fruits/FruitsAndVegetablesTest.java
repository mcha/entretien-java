package fr.ing.entretien.fruits;

import java.math.BigDecimal;
import java.util.Arrays;

import static fr.ing.entretien.fruits.Product.Category.EXOTIC;
import static fr.ing.entretien.fruits.Product.Category.LOCAL;

public class FruitsAndVegetablesTest {


    public Merchant given_merchant_Dupont() {
        Fruit banane = new Fruit("banane", new BigDecimal(2), EXOTIC);
        Fruit pomme = new Fruit("pomme", new BigDecimal(3), LOCAL);

        Fruit litchi = new Fruit("litchi", new BigDecimal(3), EXOTIC);
        Fruit poire = new Fruit("poire", new BigDecimal(4), LOCAL);

        Vegetable choux = new Vegetable("choux", new BigDecimal(2), LOCAL);
        Vegetable tapioca = new Vegetable("tapioca", new BigDecimal(4), EXOTIC);
        Vegetable carotte = new Vegetable("carotte", new BigDecimal(0.5d), LOCAL);
        Vegetable chayotte = new Vegetable("chayotte", new BigDecimal(6), EXOTIC);

        return new Merchant("Dupont", Arrays.asList(banane, pomme, litchi, poire, choux, tapioca, carotte, chayotte));
    }

    public Merchant given_merchant_Durand() {
        Fruit banane = new Fruit("banane", new BigDecimal(1.5d), EXOTIC);
        Fruit pomme = new Fruit("pomme", new BigDecimal(6), LOCAL);

        Fruit litchi = new Fruit("litchi", new BigDecimal(5), EXOTIC);
        Fruit poire = new Fruit("poire", new BigDecimal(2), LOCAL);

        Vegetable choux = new Vegetable("choux", new BigDecimal(1), LOCAL);
        Vegetable tapioca = new Vegetable("tapioca", new BigDecimal(2), EXOTIC);
        Vegetable carotte = new Vegetable("carotte", new BigDecimal(1.5d), LOCAL);
        Vegetable chayotte = new Vegetable("chayotte", new BigDecimal(1.5d), EXOTIC);

        return new Merchant("Durand", Arrays.asList(banane, pomme, litchi, poire, choux, tapioca, carotte, chayotte));
    }

    ////////////////////////// TESTS


    // Etant donnée la liste de données ci-dessus et le modèle fourni que vous pourrez enrichir

    // Ecrire une méthode retournant la moyenne des prix TTC des fruits exotiques chez Dupont
    // Refactorer cette méthode afin de pouvoir calculer également la moyenne des prix TTC des légumes exotiques chez Durand

    // Ecrire une méthode prenant une liste de marchants et qui pour une liste d'achat de 2 kilos de bananes, 2 kilos de pommes,
    // et 1 kilos de carotte, affiche le nom du marchant chez qui le panier est le moins cher
    
}

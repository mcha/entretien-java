## Fruits et légumes

Etant donnée le modèle fourni que vous pourrez enrichir : **package fr.ing.entretien.fruits**

Et le jeux de données fourni dans **FruitsAndVegetablesTest**

1. Ecrire une méthode retournant la **moyenne des prix TTC des fruits exotiques** chez Dupont

2. Refactorer cette méthode afin de pouvoir calculer également la **moyenne des prix TTC des légumes exotiques** chez Durand

3. Ecrire une méthode prenant une liste de marchants et qui pour une **liste d'achat de 2 kilos de bananes, 2 kilos de pommes,
et 1 kilos de carotte**, affiche le nom du marchant chez qui le panier est le moins cher
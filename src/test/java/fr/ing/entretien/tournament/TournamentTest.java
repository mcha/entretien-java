package fr.ing.entretien.tournament;

import java.util.List;
import java.util.Optional;

import static fr.ing.entretien.tournament.Match.match;

public class TournamentTest {

    // Etant donnée le modèle fourni que vous pourrez enrichir
    // Et le jeux de données fourni
    // Et les règles de scoring suivantes :
    // - match gagné 2 points
    // - match ex-aequo 1 points
    // - match perdu ou non joué 0 points
    // - joueur de niveau inférieur qui gagne contre un joueur de niveau supérieur = point + 2
    // - joueur de niveau inférieur qui fait ex-aequo contre un joueur de niveau supérieur = point * 2

    // Coder une méthode qui retourne le classement des joueurs au tournoi de Sydney
    // Si deux joueurs ont le même classement alors le joueur de plus faible niveau est devant

    private static final Player john = new Player("john", Player.Level.A);
    private static final Player marc = new Player("marc", Player.Level.B);
    private static final Player alan = new Player("alan", Player.Level.C);
    private static final Player jean = new Player("jean", Player.Level.D);

    private Tournament sydney() {
        var matches = List.of(match(john, alan, Match.Status.DRAW, Optional.empty()), //alan:2 - john:1
                              match(john, marc, Match.Status.WON, Optional.of(john)), //marc:0 - john:2
                              match(marc, alan, Match.Status.WON, Optional.of(marc)), //marc:2 - alan:0
                              match(jean, alan, Match.Status.DRAW, Optional.empty()), //jean:2 - alan:1
                              match(marc, jean, Match.Status.WON, Optional.of(jean)), //jean:4 - marc:0
                              match(john, jean, Match.Status.WON, Optional.of(john))  //john:2 - jean:0
                             );
        // jean:6, john:5, alan:3, marc:2
        return new Tournament("Sydney", matches);
    }
    private Tournament paris() {
        var matches = List.of(match(john, alan, Match.Status.WON, Optional.of(alan)), //alan:4 - john:0
                match(marc, alan, Match.Status.DRAW, Optional.empty()), //marc:1 - alan:2
                match(john, marc, Match.Status.WON, Optional.of(john)), //marc:0 - john:2
                match(jean, alan, Match.Status.WON, Optional.of(jean)), //jean:4 - alan:0
                match(marc, jean, Match.Status.WON, Optional.of(marc)), //jean:0 - marc:2
                match(john, jean, Match.Status.DRAW, Optional.empty())  //john:1 - jean:2
        );
        // jean:6, alan:6, marc:3, john:3
        return new Tournament("Paris", matches);
    }
}
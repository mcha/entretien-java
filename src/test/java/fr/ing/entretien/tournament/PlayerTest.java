package fr.ing.entretien.tournament;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PlayerTest {

    private static final Player john = new Player("john", Player.Level.A);
    private static final Player marc = new Player("marc", Player.Level.B);

    @Test
    void john_should_be_better_than_marc() {
        assertTrue(john.isBetterThan(marc));
    }

    @Test
    void marc_should_not_be_better_than_john() {
        assertFalse(marc.isBetterThan(john));
    }

    @Test
    void should_be_ordered_by_level() {
        final List<Player> toSort = Arrays.asList(marc, john);
        Collections.sort(toSort);
        assertEquals(john, toSort.get(0));
        assertEquals(marc, toSort.get(1));
    }


}
## Chess Tournament

Etant donnée le modèle fourni que vous pourrez enrichir : **package fr.ing.entretien.tournament**

Et le jeux de données fourni dans **TournamentTest**

Et les règles de scoring suivantes :
- match gagné 2 points
- match ex-aequo 1 points
- match perdu ou non joué 0 points
- joueur de niveau inférieur qui gagne contre un joueur de niveau supérieur = **point + 2**
- joueur de niveau inférieur qui fait ex-aequo contre un joueur de niveau supérieur = **point * 2**

Coder une méthode qui retourne le classement des joueurs au tournoi de Sydney

- Si deux joueurs ont le même classement **alors** le joueur de plus faible niveau est devant

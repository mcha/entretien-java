package fr.ing.entretien.tournament;

import fr.ing.entretien.tournament.Match.Status;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Optional;

import static fr.ing.entretien.tournament.Match.NO_WINNER_ERROR_MSG;
import static fr.ing.entretien.tournament.Match.NO_WINNER_SHOULD_BE_PROVIDED_ERROR_MSG;
import static fr.ing.entretien.tournament.Match.Status.WON;
import static fr.ing.entretien.tournament.Match.match;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MatchTest {

    private static final Player john = new Player("john", Player.Level.A);
    private static final Player marc = new Player("marc", Player.Level.B);

    @Test
    void instanciation_should_fail_when_no_winner_provided() {
        assertThatIllegalArgumentException()
                .isThrownBy(() -> match(john, marc, WON, Optional.empty()))
                .withMessage(NO_WINNER_ERROR_MSG);
    }

    @Test
    void instanciation_should_work_when_winner_provided() {
        final Match johnVSmarc = match(john, marc, WON, Optional.of(john));
        assertEquals(Optional.of(john), johnVSmarc.getWinner());
    }

    @ParameterizedTest(name = "When match status is {0}, no winner should be provided")
    @CsvSource({"TODO", "DRAW"})
    public void should_throw_when_winner_provided_and_status_not_won(Status status) {
        assertThatIllegalArgumentException()
                .isThrownBy(() -> Match.checkWinnerAgainstStatus(Optional.of(john), status))
                .withMessage(NO_WINNER_SHOULD_BE_PROVIDED_ERROR_MSG);
    }

}
### XML

Pré-requis :
> sudo apt-get install libxml2-utils

> sudo apt-get install xsltproc

#### XPath

Pour cet exercice :

* Utilisez soit le requêteur XPath de l'IDE, soit **xmllint**
* Contentez vous à la syntaxe XPath 1.0

> xmllint --shell persons.xml

> /&gt; cat xpath_expr

Le fichier qui servira pour ce test est `persons.xml`

    1. Récupérer les prénoms des personnes habitant "rue des capuccines"

> copier votre réponse ici

    2. Récupérer les prénoms des personnes majeures

> copier votre réponse ici

    3. Récupérer les prénoms des personnes sans domicile

> copier votre réponse ici

#### XSLT

Pour cet exercice :

* Utilisez soit la fonctionnalité de l'IDE, soit **xsltproc**
* Contentez vous à la syntaxe XPath 1.0

> xsltproc persons.xsl persons.xml

Ecrire les règles XSLT permettant de transformer les balises enfants de `address` en attributs de cette balise.

> Utilisez le fichier persons.xsl mis à votre disposition.

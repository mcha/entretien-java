package fr.ing.entretien.fruits;

import java.math.BigDecimal;

import static fr.ing.entretien.fruits.Product.Type.VEGETABLE;

public class Vegetable extends Product {
    public Vegetable(String name, BigDecimal kgPrice, Category category) {
        super(name, kgPrice, category, VEGETABLE);
    }
}

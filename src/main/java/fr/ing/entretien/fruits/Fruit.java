package fr.ing.entretien.fruits;

import java.math.BigDecimal;

import static fr.ing.entretien.fruits.Product.Type.FRUIT;

public class Fruit extends Product {
    public Fruit(String name, BigDecimal kgPrice, Category category) {
        super(name, kgPrice, category, FRUIT);
    }
}

package fr.ing.entretien.fruits;

import java.util.List;

public class Merchant {

    private String name;
    private List<Product> products;

    public Merchant(String name, List<Product> products) {
        this.name = name;
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public String getName() {
        return name;
    }
}

package fr.ing.entretien.fruits;

import java.math.BigDecimal;

public abstract class Product {

    protected Type type;
    protected String name;
    protected Category category;
    protected BigDecimal kgPrice;

    public Product(String name, BigDecimal kgPrice, Category category, Type type) {
        this.name = name;
        this.kgPrice = kgPrice;
        this.category = category;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getKgPrice() {
        return kgPrice;
    }

    public void setKgPrice(BigDecimal kgPrice) {
        this.kgPrice = kgPrice;
    }

    public Type getType() {
        return type;
    }

    public Category getCategory() {
        return category;
    }

    public enum Category {
        EXOTIC,
        LOCAL
    }

    public enum Type {
        FRUIT(new BigDecimal(0.20)),
        VEGETABLE(new BigDecimal(0.10));

        private BigDecimal vatRate;

        Type(BigDecimal vatRate) {
            this.vatRate = vatRate;
        }

        public BigDecimal getVatRate() {
            return vatRate;
        }
    }
}

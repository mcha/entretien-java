package fr.ing.entretien.tournament;

import java.util.List;

public class Tournament {
    private String name;
    private List<Match> matches;

    public Tournament(String name, List<Match> matches) {
        this.name = name;
        this.matches = matches;
    }

}

package fr.ing.entretien.tournament;

import java.util.Optional;

public class Match {
    public static final String NO_WINNER_ERROR_MSG = "A winner should be provided when a match is won !";
    public static final String NO_WINNER_SHOULD_BE_PROVIDED_ERROR_MSG = "A winner should not be provided when a match is not won !";
    private Player playerOne;
    private Player playerTwo;
    private Status status;

    private Optional<Player> winner;

    private Match(Player playerOne, Player playerTwo, Status status, Optional<Player> winner) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.status = status;
        this.winner = winner;
    }


    public static Match match(Player playerOne, Player playerTwo, Status status, Optional<Player> winner) {
        checkWinnerAgainstStatus(winner, status);
        return new Match(playerOne, playerTwo, status, winner);
    }

    public static void checkWinnerAgainstStatus(Optional<Player> winner, Status status) {
        switch (status) {
            case WON: winner.orElseThrow(Match::produceNoWinnerError); break;
            default: winner.ifPresent(Match::throwsNoWinnerAccepted);
        }
    }

    private static IllegalArgumentException produceNoWinnerError() {
         return new IllegalArgumentException(NO_WINNER_ERROR_MSG);
    }

    private static Player throwsNoWinnerAccepted(Player player) {
        throw new IllegalArgumentException(NO_WINNER_SHOULD_BE_PROVIDED_ERROR_MSG);
    }

    public Optional<Player> getWinner() {
        return winner;
    }

    /**
     * Match.Status
     * point is the number of points given to player for the status.
     * 0 when not played yet or lost.
     * 1 when no winner.
     * 2 for the winner when match won.
     */
    public enum Status {
        TODO(0),
        WON(2),
        DRAW(1);

        private int point;

        Status(int point) {
            this.point = point;
        }

        public int getPoint() {
            return point;
        }
    }

}

package fr.ing.entretien.tournament;

public class Player implements Comparable<Player> {
    private String name;
    private Level level;

    public Player(String name, Level level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public boolean isSameLevelAs(Player other) {
        return this.getLevel().equals(other.getLevel());
    }

    public boolean isBetterThan(Player other) {
        return this.getLevel().getValue()<other.getLevel().getValue();
    }
    
    @Override
    public int compareTo(Player other) {
        return this.getLevel().compareTo(other.getLevel());
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", level=" + level +
                '}';
    }

    /**
     * Player.Level
     * Lower value means better.
     * Ex: A is better than B.
     */
    public enum Level {
        A(1),
        B(2),
        C(3),
        D(4);

        private int value;

        Level(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
